<?php 
session_start();
include_once("layout.php");
include_once("classes/symp.php");


 ?>
   <!--  // MAIN CONTENT -->
  <body>
    <div class="page-container"  style="top: 0px; ">
        <div class="main-content" style="padding: 20px 20px 20px 20px;">
            <H3 class ="text-center">HEALTH DECLARATION FORM</H3><br/>
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                        <input id="idno" value="<?php echo @$_SESSION['User']; ?>" hidden> 
                        <input id="fname" value="<?php echo @$_SESSION['fname']; ?>" hidden> 
                        <div id="success" class="alert alert-success alert-dismissible" role="alert" hidden>
                            <strong>Success!</strong> Health Declaration successfully submitted!
                        </div>

                        <div id="warning" class="alert alert-warning alert-dismissible" role="alert" hidden>
                            <strong>Warning!</strong>   Please complete necessary details!
                        </div>

                        <div id="danger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Temperature Date already exist!
                        </div>

                        <table><tr>
                        <div class="form-group">
                            <td><label>Select Date: </label></td>
                            <td><input class="au-input" type="date" id="tempdate" name="tempdate" max="<?php echo date("Y-m-d"); ?>" style="width: 100%; background-color: white;" /></td>
                        </div></tr> 
                        <tr>
                        <div class="form-group">
                            <td style="padding-right: 8px;"><label>Temperature: </label></td>
                            <td><input class="au-input" type="number" step="any" id="temp" name="temp" placeholder="Temparature" style="width: 100%" /></td>      
                        </div></tr>
                       </table> 
                        <div class="card col-md-6">
                            <br/>
                            <div class="card-heading" style="color:#0277bd;">Health Details</div>
                            <div class="card-body">
                              

                            <?php $Symp = Symp::GetAllSymp();
                                    for ($i = 0; $i < count($Symp); $i++) {
                                    ?>
                                       <form>
                                        <div class="form-group">
                                            <label><?php echo $Symp[$i]->getsymp(); ?>:</label>
                                            
                                        </div>

                                        <div class="form-group">

                                            <select name="<?php echo $Symp[$i]->getsymp(); ?>" id="<?php echo $Symp[$i]->getsymp(); ?>" class="form-control" required="required">
                                            <option></option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                            <option value="Mild">Mild</option>
                                          </select>
                                        </div>
                                    </form>
                                    <?php
                                    }
                            ?>
                            <div class="form-group">
                                    <label>Others/Remarks:<BR><h6 style="font-weight: normal;color:#0277bd;">(If None, Indicate NA)</h6></label>
                                    <input class="au-input au-input--full" type="text" id="others" name="others" placeholder="Other Illness" />
                                </div>

                        </div>
                        <br/>
                        <br/>
                         <div id="success2" class="alert alert-success alert-dismissible" role="alert" hidden>
                            <strong>Success!</strong> Health Declaration successfully submitted!
                        </div>

                        <div id="warning2" class="alert alert-warning alert-dismissible" role="alert" hidden>
                            <strong>Warning!</strong>   Please complete necessary details!
                        </div>

                        <div id="danger2" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Temperature Date already exist!
                        </div>
                        </div>
                        <button class="btn btn-success emp-btn" id ="subhdf" type="button">Submit</button><br/>

                       




                    
                </div>
            </div>
        </div>
    </div>
</body>
   <!--  //END MAIN CONTENT
    //END PAGE CONTAINER -->
<?php include_once("footer.php"); ?>

<script src="../assets/js/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

$("#subhdf").click(function() {

if(document.getElementById("tempdate").value=='' || document.getElementById("temp").value=='' || document.getElementById("Fever").value=='' || document.getElementById("Chills").value=='' || document.getElementById("Cough").value=='' || document.getElementById("Sore Throat").value=='' || document.getElementById("Shortness of Breath").value=='' || document.getElementById("Myalgia/Body Pains").value=='' || document.getElementById("Diarrhea/LBM").value=='' || document.getElementById("others").value=='')
    {
        document.getElementById("success").setAttribute("hidden","");
        document.getElementById("warning").setAttribute("hidden","");
        document.getElementById("danger").setAttribute("hidden","");
        document.getElementById("warning").removeAttribute("hidden");
        document.getElementById("success2").setAttribute("hidden","");
        document.getElementById("warning2").setAttribute("hidden","");
        document.getElementById("danger2").setAttribute("hidden","");
        document.getElementById("warning2").removeAttribute("hidden");
    }
    else
    {
        var xmlhttp = new XMLHttpRequest();
         xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var result = this.responseText;
            var res = result.split("_"); 
           //alert(result);
           if(res[0]=='success')
           {
                document.getElementById("tempdate").value='';
                document.getElementById("temp").value='';
                document.getElementById("Fever").value='';
                document.getElementById("Chills").value='';
                document.getElementById("Cough").value='';
                document.getElementById("Sore Throat").value='';
                document.getElementById("Shortness of Breath").value='';
                document.getElementById("Myalgia/Body Pains").value='';
                document.getElementById("Diarrhea/LBM").value='';
                document.getElementById("others").value='';
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("success").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("success2").removeAttribute("hidden");
           }

           else
           {
                document.getElementById("tempdate").value='';
                document.getElementById("temp").value='';
                document.getElementById("Fever").value='';
                document.getElementById("Chills").value='';
                document.getElementById("Cough").value='';
                document.getElementById("Sore Throat").value='';
                document.getElementById("Shortness of Breath").value='';
                document.getElementById("Myalgia/Body Pains").value='';
                document.getElementById("Diarrhea/LBM").value='';
                document.getElementById("others").value='';
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("danger").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("danger2").removeAttribute("hidden");
           }
        
           

        }

      };

      xmlhttp.open("GET", "./php/submithdf.php?idno="+document.getElementById("idno").value+"&fname="+document.getElementById("fname").value+"&temp="+document.getElementById("temp").value+"&tempdate="+document.getElementById("tempdate").value+"&fever="+document.getElementById("Fever").value+"&chills="+document.getElementById("Chills").value+"&cough="+document.getElementById("Cough").value+"&sorethroat="+document.getElementById("Sore Throat").value+"&sob="+document.getElementById("Shortness of Breath").value+"&bodypain="+document.getElementById("Myalgia/Body Pains").value+"&lbm="+document.getElementById("Diarrhea/LBM").value+"&others="+document.getElementById("others").value, true);
      xmlhttp.send();
    }



});

$("#tempdate").datepicker({
        endDate:'today'
});



//        End----------------------------------------------------------------------------------------------------------------------------------

});
</script>
