<?php 
session_start();
include_once("../layout.php");
include_once($_SERVER['DOCUMENT_ROOT']."/HDF/classes/question.php");

 ?>
   <!--  // MAIN CONTENT -->
  <body>

    <div class="page-container"  style="top: 0px; ">
        <div class="main-content" style="padding: 20px 20px 20px 20px;">
            <H3 class ="text-center">Recent Activities</H3><br/>
             <div class="section__content section__content--p30">
                <div class="container-fluid col-md-6" style="align-self: center;">
                    <input id="idno" value="<?php echo @$_SESSION['User']; ?>" hidden> 
                    <input id="fname" value="<?php echo @$_SESSION['fname']; ?>" hidden> 
                    <div id="success" class="alert alert-success alert-dismissible" role="alert" hidden>
                        <strong>Success!</strong> Recent Activities successfully submitted!
                    </div>

                    <div id="warning" class="alert alert-warning alert-dismissible" role="alert" hidden>
                        <strong>Warning!</strong>   Please complete necessary details!
                    </div>

                    <div id="danger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                            <strong>Error!</strong>  Can't process request!
                    </div>

                    <div class="card col-md-12" style="align-self: center;">
                        <br/>
                        <!-- <div class="card-heading" style="color:#0277bd;align-self: center;">Recent Activities</div> -->
                        <div>
                        <div class="card-body">
                               
                            <?php $Ques = Ques::GetAllQues();
                                    for ($i = 0; $i < count($Ques); $i++) {
                                    ?>
                                       <form>
                                        <div class="form-group">
                                            <label><?php echo $Ques[$i]->getqdesc(); ?></label>
                                            
                                        </div>

                                        <div class="form-group">

                                            <select name="<?php echo $Ques[$i]->getqcode(); ?>" id="<?php echo $Ques[$i]->getqcode(); ?>" class="form-control" required="required" onchange="Selected(this)">
                                            <option></option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                          </select>
                                        </div>
                                        
                                        <div class="form-group" <?php if($Ques[$i]->getqremarks()==0) {  ?> hidden  <?php } ?>>
                                            <label><?php echo $Ques[$i]->getrdesc(); ?><BR></label>
                                            <input class="au-input au-input--full" type="text" id="<?php $i; ?>" name="<?php $i; ?>" value="<?php if($Ques[$i]->getqremarks()==0) { echo "NA"; } ?>" />
                                        </div>
                                       
                                        <?php if($i==3)
                                        { ?>
                                            <h3><strong>Other activities within 14 days up to present:</strong></h3><br/>
                                       <?php }
                                        ?>
                                    </form>
                                    <?php
                                    }
                            ?>
                        </div>


                        </div>
                        <br/>
                        <br/>
                         <div id="success2" class="alert alert-success alert-dismissible" role="alert" hidden>
                            <strong>Success!</strong> Recent Activities successfully submitted!
                        </div>

                        <div id="warning2" class="alert alert-warning alert-dismissible" role="alert" hidden>
                            <strong>Warning!</strong>   Please complete necessary details!
                        </div>

                        <div id="danger2" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Can't process request!
                        </div>
                        </div>
                        <button class="btn btn-success emp-btn" id ="subract" type="button">Submit</button><br/>
                        
                    </div>


                </div>
             </div>

        </div>
    </div>
</body>
   <!--  //END MAIN CONTENT
    //END PAGE CONTAINER -->
<?php include_once("../footer.php"); ?>

<script src="../assets/js/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript">
var YM = [];
var Symp = [];
var Remarks = [];
$(document).ready(function() {

$("#subract").click(function() {
Remarks =[];
$("input[type='text']").each(function () {  

    Remarks.push($(this).val());


})  


 var elements = document.getElementsByTagName("select");
                    for (var ii=0; ii < elements.length; ii++) {
                        elements[ii].value = "";
                    }

if(Remarks.length!=YM.length || Remarks.length != Symp.length)
    {
        document.getElementById("success").setAttribute("hidden","");
        document.getElementById("warning").setAttribute("hidden","");
        document.getElementById("danger").setAttribute("hidden","");
        document.getElementById("warning").removeAttribute("hidden");
        document.getElementById("success2").setAttribute("hidden","");
        document.getElementById("warning2").setAttribute("hidden","");
        document.getElementById("danger2").setAttribute("hidden","");
        document.getElementById("warning2").removeAttribute("hidden");
    }
    else
    {
        var xmlhttp = new XMLHttpRequest();
         xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var result = this.responseText;
            var res = result.split("_"); 
           //alert(result);
           if(res[0]=='success')
           {
                var text = document.getElementsByTagName("input");
                    for (var ii=0; ii < text.length; ii++) {
                      if (text[ii].type == "text") {
                        text[ii].value = "";
                      }
                    }

                    var elements = document.getElementsByTagName("select");
                    for (var ii=0; ii < elements.length; ii++) {
                        elements[ii].value = "";
                    }
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("success").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("success2").removeAttribute("hidden");

           }

           else
           {

                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("danger").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("danger2").removeAttribute("hidden");
           }
        
           

        }

      };

      xmlhttp.open("GET", "../php/submitact2.php?idno="+document.getElementById("idno").value+"&fname="+document.getElementById("fname").value+"&YM="+JSON.stringify(YM)+"&Ques="+JSON.stringify(Symp)+"&QR="+JSON.stringify(Remarks), true);
      xmlhttp.send();
    }



});





//        End----------------------------------------------------------------------------------------------------------------------------------

});

function Selected(select) {
    

    if(Symp.indexOf(select.id)!==-1)
    {
            
        YM[Symp.indexOf(select.id)]=select.options[select.selectedIndex].text;
            
    }
    else
    {
            YM.push(select.options[select.selectedIndex].text);
            Symp.push(select.id);
            
    }
    //alert(JSON.stringify(YM) + JSON.stringify(Symp));   
}



</script>
