
<?php
session_start();
 include_once("../layout.php"); 
include_once($_SERVER['DOCUMENT_ROOT']."/HDF/classes/Reg.php");
$root= $_SERVER['DOCUMENT_ROOT'];
$Reg = new Reg();
$Reg->UserData($_SESSION['User']); 
$add = $Reg->getadd();
$contact = $Reg->getcontact(); 

?>


<main class="mdl-layout__content mdl-color--grey-100" >
<div class="page-wrapper" style="background: #607d8b;">
        
    <div class="container-fluid" style="background: #607d8b;">
        <div class="container">
            <div class="login-wrap" style="padding-top: 0vh; margin-top: 8vh; padding-bottom: 20px;">
                <div class="login-content">
                    <!-- <div class="login-logo"><a href="#"><img src="../assets/images/icon/ionics.png" alt="Ionics" style="height:100px;"/></a></div> -->
                    <h3 align="center">Edit Profile</h3></br>
                    <div class="login-form">
                        <?php if(@$_GET['Success'] == true){ ?>
                        <div id="success2" class="alert alert-success alert-dismissible" role="alert">
                                <strong>Success!</strong>  <?php echo @$_GET['Success']; ?>!
                        </div>
                        <?php } ?>

                        <?php if(@$_GET['Error'] == true){ ?>
                        <div id="success" class="alert alert-danger alert-dismissible" role="alert">
                                <strong>Error!</strong>  <?php echo @$_GET['Error']; ?>!
                        </div>
                        <?php } ?>
                        <div id="upload-form">
                                
                                <div class="form-group"><label>Upload Photo</label></div>
                                <form action="../php/upload.php" method="POST" enctype="multipart/form-data">
                                    <input type="file" name="file"><br/>
                                    <button type="submit" name="submit" style="margin: 15px 15px 15px 0px;" class="btn btn-success emp-btn">UPLOAD</button>
                                    
                                </form>
                            </div>
                        <form method="POST">

                            <div id="warning" class="alert alert-warning alert-dismissible" role="alert" hidden>
                                <strong>Warning!</strong>   Please complete necessary details!
                            </div>



                            <div id="success" class="alert alert-success alert-dismissible" role="alert" hidden>
                                <strong>Success!</strong>  Profile Successfully Updated!
                            </div>
                            

                            <br/>
                            <div id="details-form">
                                <div class="form-group">
                                    <label>Address Type:</label>
                                    <Select class="form-control" id="addtype" name="addtype" autofocus>
                                         <option></option>
                                         <option value="permanent">Permanent Address</option>
                                         <option value="rent">Boarding House</option>
                                    </Select>
                                </div>
                                <div class="form-group">
                                    <label>Address:</label>
                                    <input class="au-input au-input--full" type="text" id="add" name="add" value="<?php echo $add; ?>" />
                                </div>
                                <div class="form-group">
                                    <label>Contact Number:</label>
                                    <input class="au-input au-input--full" type="tel" id="contact" name="contact" value="<?php echo $contact; ?>" maxlength="11" />
                                </div>
                                <button class="btn btn-success emp-btn" id ="update" type="button">Update Profile</button>

                            </div>
                            </div>
                            

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</main>

<?php include_once("../publicfooter.php"); ?>

<script type="text/javascript">
$(document).ready(function() {



$("#update").click(function() {

        
        var url = window.location.origin;
        
        if(document.getElementById("addtype").value=='' || document.getElementById("add").value=='' || document.getElementById("contact").value=='')
        {
            document.getElementById("success").setAttribute("hidden","");
            document.getElementById("warning").setAttribute("hidden","");
            document.getElementById("warning").removeAttribute("hidden");
        }
        else
        {
            var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
            var result = this.responseText;
            var res = result.split("_"); 
            //alert(result);
            document.getElementById("contact").value='';
            document.getElementById("add").value='';
            document.getElementById("addtype").value='';
            document.getElementById("success").setAttribute("hidden","");
            document.getElementById("warning").setAttribute("hidden","");
            document.getElementById("success").removeAttribute("hidden");

            }

          };

          xmlhttp.open("GET", "../php/updateprof.php?addtype="+document.getElementById("addtype").value+"&add="+document.getElementById("add").value+"&contact="+document.getElementById("contact").value, true);
          xmlhttp.send();
            
            

        }
      
  


});



//        End----------------------------------------------------------------------------------------------------------------------------------

});
</script>




