<?php 
session_start();
include_once("../layout.php");
include_once($_SERVER['DOCUMENT_ROOT']."/HDF/classes/symp.php");

 ?>
   <!--  // MAIN CONTENT -->
  <body>

    <div class="page-container"  style="top: 0px; ">
        <div class="main-content" style="padding: 20px 20px 20px 20px;">
            <H3 class ="text-center">Contact Tracing</H3><br/>
             <div class="section__content section__content--p30">
                <div class="container-fluid col-md-6" style="align-self: center;">
                    <input id="idno" value="<?php echo @$_SESSION['User']; ?>" hidden> 
                    <input id="fname" value="<?php echo @$_SESSION['fname']; ?>" hidden> 
                    <div id="success" class="alert alert-success alert-dismissible" role="alert" hidden>
                        <strong>Success!</strong> successfully submitted!
                    </div>

                    <div id="warning" class="alert alert-warning alert-dismissible" role="alert" hidden>
                        <strong>Warning!</strong>   Please complete necessary details!
                    </div>

                    <div id="danger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                            <strong>Error!</strong>   Can't process request!
                    </div>

                    <div class="card col-md-12" style="align-self: center;">
                        <br/>
                        <!-- <div class="card-heading" style="color:#0277bd;align-self: center;">Recent Activities</div> -->
                        <div class="card-body">
                            <form>
                                <div class="form-group">
                                    <label>Name:<BR></label>
                                    <input class="au-input au-input--full" type="text" id="name" name="name" />
                                </div>
                            </form>

                            <form>

                                <div class="form-group">
                                    <label>Age:<BR></label>
                                    <input class="au-input au-input--full" type="number" id="age" name="age" />
                                </div>
                            </form>

                            <form>
                                <div class="form-group">
                                    <label>Gender:</label>
                                    
                                </div>

                                <div class="form-group">
                                    <select name="gender" id="gender" class="form-control" required="required">
                                    <option></option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                  </select>
                                </div>
                            </form>
                            <form>
                                <div class="form-group">
                                    <label>Place:</label>
                                    
                                </div>

                                <div class="form-group">

                                    <select name="place" id="place" class="form-control" required="required">
                                    <option></option>
                                    <option value="Home">Home</option>
                                    <option value="Apartment">Boarding House/Apartment</option>
                                    <option value="Canteen">Canteen</option>
                                    <option value="Workplace">Workplace</option>
                                    <option value="Meeting">Meeting</option>
                                    <option value="Seminar">Seminar</option>
                                    <option value="Shuttle">Shuttle</option>
                                    <option value="Outside Activities">Outside Activities</option>
                                  </select>
                                </div>
                            </form>

                             <form>
                                <div class="form-group">
                                    <label>Date:</label>
                                </div>

                                <div class="form-group">
                                    <input class="au-input au-input--full" type="date" id="datetime" name="datetime" />
                                </div>
                            </form>

                            <form>
                                <div class="form-group">
                                    <label>Relationship:</label>
                                    
                                </div>

                                <div class="form-group">

                                    <select name="relationship" id="relationship" class="form-control" required="required">
                                    <option></option>
                                    <option value="Friend">Friend</option>
                                    <option value="Family">Family</option>
                                    <option value="Officemate">Officemate</option>
                                    <option value="Others">Others</option>
                                  </select>
                                </div>
                            </form>

                            

                            <form>
                                <div class="form-group">
                                    <label>Company:<BR></label>
                                    <input class="au-input au-input--full" type="text" id="company" name="company" />
                                </div>
                            </form>

                            <form>
                                <div class="form-group">
                                    <label>Address:<BR></label>
                                    <input class="au-input au-input--full" type="text" id="address" name="address" />
                                </div>
                            </form>

                            <form>
                                <div class="form-group">
                                    <label>Activity:<BR></label>
                                    <input class="au-input au-input--full" type="text" id="activity" name="activity" />
                                </div>
                            </form>

                          
                            
                            


                        </div>
                        <br/>
                        <br/>
                         <div id="success2" class="alert alert-success alert-dismissible" role="alert" hidden>
                            <strong>Success!</strong> successfully submitted!
                        </div>

                        <div id="warning2" class="alert alert-warning alert-dismissible" role="alert" hidden>
                            <strong>Warning!</strong>   Please complete necessary details!
                        </div>

                        <div id="danger2" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong> Can't process request!
                        </div>
                        </div>
                        <button class="btn btn-success emp-btn" id ="subctracing" type="button" onclick="topFunction()" >Submit</button><br/>
                    </div>


                </div>
             </div>

        </div>
    </div>
</body>
   <!--  //END MAIN CONTENT
    //END PAGE CONTAINER -->
<?php include_once("../footer.php"); ?>

<script src="../assets/js/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

$("#subctracing").click(function() {
if(document.getElementById("place").value=='' || document.getElementById("datetime").value=='' || document.getElementById("relationship").value=='' || document.getElementById("gender").value=='' || document.getElementById("company").value=='' || document.getElementById("address").value=='' || document.getElementById("activity").value=='' || document.getElementById("age").value=='' || document.getElementById("name").value=='')
    {
        document.getElementById("success").setAttribute("hidden","");
        document.getElementById("warning").setAttribute("hidden","");
        document.getElementById("danger").setAttribute("hidden","");
        document.getElementById("warning").removeAttribute("hidden");
        document.getElementById("success2").setAttribute("hidden","");
        document.getElementById("warning2").setAttribute("hidden","");
        document.getElementById("danger2").setAttribute("hidden","");
        document.getElementById("warning2").removeAttribute("hidden");
    }
    else
    {
        var xmlhttp = new XMLHttpRequest();
         xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var result = this.responseText;
            var res = result.split("_"); 
           //alert(result);
           if(res[0]=='success')
           {
                document.getElementById("place").value='';
                document.getElementById("datetime").value='';
                document.getElementById("relationship").value='';
                document.getElementById("name").value='';
                document.getElementById("age").value='';
                document.getElementById("gender").value='';
                document.getElementById("company").value='';
                document.getElementById("address").value='';
                document.getElementById("activity").value='';
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("success").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("success2").removeAttribute("hidden");
           }

           else
           {
                document.getElementById("place").value='';
                document.getElementById("datetime").value='';
                document.getElementById("relationship").value='';
                document.getElementById("name").value='';
                document.getElementById("age").value='';
                document.getElementById("gender").value='';
                document.getElementById("company").value='';
                document.getElementById("address").value='';
                document.getElementById("activity").value='';
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("danger").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("danger2").removeAttribute("hidden");
           }
        
           

        }

      };

      xmlhttp.open("GET", "../php/submitctracing.php?idno="+document.getElementById("idno").value+"&place="+document.getElementById("place").value+"&datetime="+document.getElementById("datetime").value+"&relationship="+document.getElementById("relationship").value+"&name="+document.getElementById("name").value+"&age="+document.getElementById("age").value+"&gender="+document.getElementById("gender").value+"&company="+document.getElementById("company").value+"&address="+document.getElementById("address").value+"&activity="+document.getElementById("activity").value, true);
      xmlhttp.send();
    }



});




//        End----------------------------------------------------------------------------------------------------------------------------------

});

var mybutton = document.getElementById("subctracing");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>
