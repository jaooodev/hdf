<?php 
session_start();
include_once("../layout.php");
include_once($_SERVER['DOCUMENT_ROOT']."/HDF/classes/symp.php");

 ?>
   <!--  // MAIN CONTENT -->
  <body>

    <div class="page-container"  style="top: 0px; ">
        <div class="main-content" style="padding: 20px 20px 20px 20px;">
            <H3 class ="text-center">VITAL SIGNS</H3><br/>
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                        <input id="idno" value="<?php echo @$_SESSION['User']; ?>" hidden> 
                        <input id="fname" value="<?php echo @$_SESSION['fname']; ?>" hidden> 
                        <div id="success" class="alert alert-success alert-dismissible" role="alert" hidden>
                            <strong>Success!</strong> Health Declaration successfully submitted!
                        </div>

                        <div id="warning" class="alert alert-warning alert-dismissible" role="alert" hidden>
                            <strong>Warning!</strong>   Please complete necessary details!
                        </div>

                        <div id="danger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Temperature Date already exist!
                        </div>

                        <table><tr>
                        <div class="form-group">
                            <td><label>Select Date: </label></td>
                            <td><input class="au-input" type="date" id="tempdate" name="tempdate" min = "<?php echo date('Y-m-d', strtotime('-1 day')); ?>" max="<?php echo date("Y-m-d"); ?>" style="width: 100%; background-color: white;" /></td>
                        </div></tr> 
                        <tr>
                        <div class="form-group">
                            <td style="padding-right: 8px;"><label>Temperature: </label></td>
                            <td><input class="au-input" type="number" step="any" id="temp" name="temp" placeholder="Temparature" style="width: 100%" /></td>      
                        </div></tr>
                       </table> 
                        <div class="card col-md-6">
                            <br/>
                            <div class="card-heading" style="color:#0277bd;">Health Details</div>
                            <div class="card-body">
                              
                                <?php $yes = array(); ?>
                            <?php $Symp = Symp::GetAllSymp();
                                    for ($i = 0; $i < count($Symp); $i++) {
                                    ?>
                                       <form>
                                        <div class="form-group">
                                            <label><?php echo $Symp[$i]->getsymp(); ?>:</label>
                                            
                                        </div>

                                        <div class="form-group">

                                            <select name="<?php echo $Symp[$i]->getsymp(); ?>" id="<?php echo $Symp[$i]->getsymp(); ?>" class="form-control" required="required" onchange="Selected(this)">
                                            <option></option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                            <option value="Mild">Mild</option>
                                          </select>
                                        </div>
                                    </form>
                                    <?php
                                    }
                            ?>
                            <div class="form-group">
                                    <label>Others/Remarks:<BR><h6 style="font-weight: normal;color:#0277bd;">(If None, Indicate NA)</h6></label>
                                    <input class="au-input au-input--full" type="text" id="others" name="others" placeholder="Other Illness" />
                                </div>

                        </div>
                        <br/>
                        <br/>
                         <div id="success2" class="alert alert-success alert-dismissible" role="alert" hidden>
                            <strong>Success!</strong> Health Declaration successfully submitted!
                        </div>

                        <div id="warning2" class="alert alert-warning alert-dismissible" role="alert" hidden>
                            <strong>Warning!</strong>   Please complete necessary details!
                        </div>

                        <div id="danger2" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Temperature Date already exist!
                        </div>
                        </div>
                        <button class="btn btn-success emp-btn" id ="subhdf" type="button">Submit</button><br/>

                       




                    
                </div>
            </div>
        </div>
    </div>
</body>
   <!--  //END MAIN CONTENT
    //END PAGE CONTAINER -->
<?php include_once("../footer.php"); ?>

<script src="../assets/js/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript">
var YM = [];
var Symp = [];
$(document).ready(function() {

$("#subhdf").click(function() {
var empty = YM.length - YM.filter(String).length;
//alert(YM.length - empty);
var total = YM.length - empty;
if(document.getElementById("tempdate").value=='' || document.getElementById("temp").value=='' || document.getElementById("others").value=='' || document.getElementsByTagName("select").length != total)
    {
        document.getElementById("success").setAttribute("hidden","");
        document.getElementById("warning").setAttribute("hidden","");
        document.getElementById("danger").setAttribute("hidden","");
        document.getElementById("warning").removeAttribute("hidden");
        document.getElementById("success2").setAttribute("hidden","");
        document.getElementById("warning2").setAttribute("hidden","");
        document.getElementById("danger2").setAttribute("hidden","");
        document.getElementById("warning2").removeAttribute("hidden");
    }
    else
    {
        var xmlhttp = new XMLHttpRequest();
         xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var result = this.responseText;
            var res = result.split("_"); 
           //alert(result);
           if(res[0]=='success')
           {
                var text = document.getElementsByTagName("input");
                    for (var ii=0; ii < text.length; ii++) {
                      if (text[ii].type == "text") {
                        text[ii].value = "";
                      }
                    }

                    var elements = document.getElementsByTagName("select");
                    for (var ii=0; ii < elements.length; ii++) {
                        elements[ii].value = "";
                    }
                YM =[];
                Symp =[];
                document.getElementById("tempdate").value='';
                document.getElementById("temp").value='';
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("success").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("success2").removeAttribute("hidden");
           }

           else
           {

                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("danger").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("danger2").removeAttribute("hidden");
           }
        
           

        }

      };

      xmlhttp.open("GET", "../php/emphdf.php?idno="+document.getElementById("idno").value+"&fname="+document.getElementById("fname").value+"&temp="+document.getElementById("temp").value+"&tempdate="+document.getElementById("tempdate").value+"&others="+document.getElementById("others").value+"&YM="+JSON.stringify(YM)+"&Symp="+JSON.stringify(Symp), true);
      xmlhttp.send();
    }



});




//        End----------------------------------------------------------------------------------------------------------------------------------

});

/*function Selected(select) {
    
    if(select.options[select.selectedIndex].text == 'Yes' || select.options[select.selectedIndex].text == 'Mild')
    {
        if(Symp.indexOf(select.id)!==-1)
        {
            
            if(select.options[select.selectedIndex].text == 'Yes' || select.options[select.selectedIndex].text == 'Mild')
            {
                YM[Symp.indexOf(select.id)]=select.options[select.selectedIndex].text;
            }
        }
        else
        {
            YM.push(select.options[select.selectedIndex].text);
            Symp.push(select.id);
            
        }
        
    }
    else if (select.options[select.selectedIndex].text=='No')
    {
        if(Symp.indexOf(select.id)!==-1)
        {
            delete YM[Symp.indexOf(select.id)];
            delete Symp[Symp.indexOf(select.id)];
        }     

    }
    alert(JSON.stringify(YM) + JSON.stringify(Symp));
    
     
}*/

function Selected(select) {
    

    if(Symp.indexOf(select.id)!==-1)
    {
        
        YM[Symp.indexOf(select.id)]=select.options[select.selectedIndex].text;
            
    }
    else
    {
            YM.push(select.options[select.selectedIndex].text);
            Symp.push(select.id);
            
    }
    //alert(JSON.stringify(YM) + JSON.stringify(Symp));   
}


    
     

</script>
