<?php 
session_start();
include_once("../layout.php");
include_once("../classes/CTRACING.php");

 ?>
   <!--  // MAIN CONTENT -->
  <body>

    <div class="page-container"  style="top: 0px; ">
        <div class="main-content" style="padding: 20px 20px 20px 20px;">
            <H3 class ="text-center">Contact Tracing Reports</H3><br/>
            <p class="text-center">Last HDF Declaration: <?php echo CTRACING::Lastupdate($_SESSION['User']); ?></p>
             <div class="section__content section__content--p30">
                <div class="container-fluid col-md-6" style="align-self: center;">

                   

                    <div class="card col-md-12" style="align-self: center;">

                        <div class="card-body">

                          <table class="table" style="font-size: 12px">
                              <thead>
                                <tr>
                                  <th scope="col">Name</th>
                                  <th scope="col">Date</th>
                                  <th scope="col">Details</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php $ctracing = CTRACING::Tracing($_SESSION['User']);
                              for ($i=0; $i < count($ctracing); $i++) 
                                { 
                                
                                ?>

                                <tr>
                                    <td><?php echo $ctracing[$i]->getname(); ?></td>
                                    <td><?php echo $ctracing[$i]->getdatetime(); ?></td>
                                    <td><button style="font-size: 12px;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#<?php echo $ctracing[$i]->getid(); ?>">VIEW</button></td>
                                    

                                      <!-- Modal -->
                                      <div class="modal fade" id="<?php echo $ctracing[$i]->getid(); ?>" role="dialog" data-backdrop="false" style="z-index: 0; padding-top: 200px;height: 1000px">
                                        <div class="modal-dialog">
                                        
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Contact Tracing Details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Name:   <strong><?php echo $ctracing[$i]->getname(); ?></strong></p>
                                                <p>Place:   <strong><?php echo $ctracing[$i]->getplace(); ?></strong></p>
                                                <p>Date:   <strong><?php echo $ctracing[$i]->getdatetime(); ?></strong></p>
                                                <p>Relationship:   <strong><?php echo $ctracing[$i]->getrelationship(); ?></strong></p>
                                                <p>Age:   <strong><?php echo $ctracing[$i]->getage(); ?></strong></p>
                                                <p>Gender:   <strong><?php echo $ctracing[$i]->getgender(); ?></strong></p>
                                                <p>Company:   <strong><?php echo $ctracing[$i]->getcompany(); ?></strong></p>
                                                <p>Address:   <strong><?php echo $ctracing[$i]->getaddress(); ?></strong></p>
                                                <p>Activity:   <strong><?php echo $ctracing[$i]->getactivity(); ?></strong></p>
                                                
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                          </div>
                                          
                                        </div>
                                      </div>
                                      
                                    
                                <?php } ?>
                                </tr>
                              </tbody>
                            </table>
                           
                            


                        </div>
                     
                    </div>
                    


                </div>
             </div>

        </div>
    </div>
</body>
   <!--  //END MAIN CONTENT
    //END PAGE CONTAINER -->
<?php include_once("../footer.php"); ?>
<script src="../assets/js/jquery-3.2.1.slim.min.js"></script>
<script>
$(document).ready(function(){
    $(".modal").on('shown.bs.modal', function(){
        $(this).find('#inputName').focus();
    });
});
</script>

