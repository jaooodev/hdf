<?php 
session_start();
include_once("../layout.php");
include_once("../classes/CTRACING.php");
include_once("../classes/ActiveEmployee.php");

 ?>
  <body>

    <div class="page-container"  style="top: 0px; ">
        <div class="main-content" style="padding: 20px 20px 20px 20px;">
            <H3 class ="text-center">Employees Report</H3><br/>
             <div class="section__content section__content--p30">
                <div class="container-fluid col-md-6" style="align-self: center;">
                  <div class="col-md-3">
                      <label>Department:</label>
                  </div>
                   <div class="col-md-3">
                      <select id="cmbrejectcat" class="form-control input-sm" name="cmbrejectcat">
                        <option></option>
                        <?php
                        $emp = Idnum::AllDept();
                        for ($i = 0; $i < count($emp); $i++) {
                        ?>
                          <option><?php echo $emp[$i]->getdept(); ?></option>
                        <?php } ?>
                      </select><br>
                    </div>

                    <div class="card col-md-12" style="align-self: center;">

                        <div class="card-body">

                          <table class="table" style="font-size: 14px">
                              <thead>
                                <tr>
                                  <th scope="col">Name</th>
                                  <th scope="col">Temp</th>
                                  <th scope="col">Date</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                              </tbody>
                            </table>
                           
                            


                        </div>
                     
                    </div>
                    


                </div>
             </div>

        </div>
    </div>
</body>
<?php include_once("../footer.php"); ?>

