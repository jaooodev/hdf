
<?php include_once("header.php"); ?>


<main class="mdl-layout__content mdl-color--grey-100" >
<div class="page-wrapper" style="background: #607d8b;">
        
    <div class="container-fluid" style="background: #607d8b;">
        <div class="container">
            <div class="login-wrap" style="padding-top: 0vh; margin-top: 8vh; padding-bottom: 20px;">
                <div class="login-content">
                    <div class="login-logo"><a href="#"><img src="assets/images/icon/ionics.png" alt="Ionics" style="height:100px;"/></a></div>
                    <div class="login-form">
                        <form method="POST">


                            <div id="warning" class="alert alert-warning alert-dismissible" role="alert" hidden>
                                <strong>Warning!</strong>   Please complete necessary details!
                            </div>

                            <div id="danger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Password did not match!
                            </div>

                            <div id="iddanger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   ID Number not found!
                            </div>

                            <div id="exist" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   ID Number already registered!
                            </div>
                            
                            <div id="first-form">
                                <div class="form-group"><label>ID Number</label><input class="au-input" type="text" id="idno" name="idno" placeholder="ID" size="14" style="width: 50%" />
                                    <button type="button" class="au-input float-right au-btn--green m-b-20" id="checkID"><p style="color: white">Check ID</p></button>
                                </div>

                                <div class="form-group"><label>Name</label><input class="au-input au-input--full" type="text" id="fname" name="fname" placeholder="Full Name" disabled="" /></div>
                                <div class="form-group"><label>Department</label><input class="au-input au-input--full" type="text" id="dept" name="dept" placeholder="Department" disabled="" /></div>
                                <div class="form-group"><label>Sub Department</label><input class="au-input au-input--full" type="text" id="subdept" name="subdept" placeholder="Sub Department" disabled="" /></div>
                            </div>

                            <div id="second-form">
                                <div class="form-group">
                                    <label>Address Type:</label>
                                    <Select class="form-control" id="addtype" name="addtype" autofocus>
                                         <option></option>
                                         <option value="permanent">Permanent Address</option>
                                         <option value="rent">Boarding House</option>
                                    </Select>
                                </div>
                                <div class="form-group">
                                    <label>Address:</label>
                                    <input class="au-input au-input--full" type="text" id="add" name="add" placeholder="Address" />
                                </div>
                                <div class="form-group">
                                    <label>Contact Number:</label>
                                    <input class="au-input au-input--full" type="tel" id="contact" name="contact" placeholder="Contact Number" maxlength="11" />
                                </div>
                                <div class="form-group"><label>Password</label><input class="au-input au-input--full" type="password" id="password1" name="password" placeholder="Password" /></div>

                                <div class="form-group"><label>Confirm Password</label><input class="au-input au-input--full" type="password" id="password2" name="password" placeholder="Confirm Password" /></div>
                                <button class="btn btn-success emp-btn" id ="reg" type="button" >Register</button>
                                <div style="text-align:right;"><a href="index.php" style="color:red;font-size: 12px;">Return to Login Page</a></div>
                                <div id="success" class="alert alert-success alert-dismissible" role="alert" hidden>
                                <strong>Success!</strong> ID Number is successfully registered!
                                <button class="btn btn-success emp-btn" id ="reg" type="button"><a href="index.php">Login Account</a></button>

                            </div>
                            </div>
                            

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</main>

<?php include_once("footer.php"); ?>
<script src="assets/js/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$("#idno").focus();
$("#checkID").click(function() {

if(document.getElementById("idno").value=='')
{
    document.getElementById("success").setAttribute("hidden","");
    document.getElementById("exist").setAttribute("hidden","");
    document.getElementById("warning").setAttribute("hidden","");
    document.getElementById("danger").setAttribute("hidden","");
    document.getElementById("iddanger").setAttribute("hidden","");
    document.getElementById("iddanger").removeAttribute("hidden");
}
else
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var result = this.responseText;
        var res = result.split("_"); 

        
        if(res[0]=='ok') 
        {
            document.getElementById("idno").value = res[4];
            document.getElementById("fname").value = res[1];
            document.getElementById("dept").value = res[2];
            document.getElementById("subdept").value = res[3];
            document.getElementById("reg").disabled=false;
            document.getElementById("success").setAttribute("hidden","");
            document.getElementById("warning").setAttribute("hidden","");
            document.getElementById("danger").setAttribute("hidden","");
            document.getElementById("iddanger").setAttribute("hidden","");
            document.getElementById("exist").setAttribute("hidden","");

        }
        else if(res[0]=='notexist')
        {
            document.getElementById("success").setAttribute("hidden","");
            document.getElementById("warning").setAttribute("hidden","");
            document.getElementById("danger").setAttribute("hidden","");
            document.getElementById("exist").setAttribute("hidden","");
            document.getElementById("iddanger").setAttribute("hidden","");
            document.getElementById("iddanger").removeAttribute("hidden");
        }
        else if(res[0]=='exist')
        {
            document.getElementById("success").setAttribute("hidden","");
            document.getElementById("warning").setAttribute("hidden","");
            document.getElementById("danger").setAttribute("hidden","");
            document.getElementById("exist").setAttribute("hidden","");
            document.getElementById("iddanger").setAttribute("hidden","");
            document.getElementById("exist").removeAttribute("hidden");

        }

    }

  };

  xmlhttp.open("GET", "./php/checkID.php?idno="+document.getElementById("idno").value, true);
  xmlhttp.send();
}
    

});

$("#reg").click(function() {

      if(document.getElementById("password1").value==document.getElementById("password2").value)
        {
            if(document.getElementById("addtype").value=='' || document.getElementById("add").value=='' || document.getElementById("contact").value=='' || document.getElementById("password1").value=='')
            {
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("exist").setAttribute("hidden","");
                document.getElementById("iddanger").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("warning").removeAttribute("hidden");
            }
            else
            {
                var xmlhttp = new XMLHttpRequest();
              xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                var result = this.responseText;
                var res = result.split("_"); 
                document.getElementById("password1").value='';
                document.getElementById("password2").value='';
                document.getElementById("contact").value='';
                document.getElementById("add").value='';
                document.getElementById("addtype").value='';
                document.getElementById("subdept").value='';
                document.getElementById("dept").value='';
                document.getElementById("fname").value='';
                document.getElementById("idno").value='';
                document.getElementById("checkID").disabled=false;
                document.getElementById("reg").disabled=true;
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("exist").setAttribute("hidden","");
                document.getElementById("iddanger").setAttribute("hidden","");
                document.getElementById("success").removeAttribute("hidden");

                }

              };

              xmlhttp.open("GET", "./php/reg.php?idno="+document.getElementById("idno").value+"&fname="+document.getElementById("fname").value+"&dept="+document.getElementById("dept").value+"&subdept="+document.getElementById("subdept").value+"&addtype="+document.getElementById("addtype").value+"&add="+document.getElementById("add").value+"&contact="+document.getElementById("contact").value+"&password1="+document.getElementById("password1").value, true);
              xmlhttp.send();
                
                

            }
        }
        else
        {
            document.getElementById("success").setAttribute("hidden","");
            document.getElementById("warning").setAttribute("hidden","");
            document.getElementById("exist").setAttribute("hidden","");
            document.getElementById("iddanger").setAttribute("hidden","");
            document.getElementById("danger").setAttribute("hidden","");
            document.getElementById("danger").removeAttribute("hidden");

        }
  


});



//        End----------------------------------------------------------------------------------------------------------------------------------

});
</script>




