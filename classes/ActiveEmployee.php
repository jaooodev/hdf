<?php

include_once("connection2.php");

class Idnum
{
	private $empcode;
	private $empname;
	private $dept;
	private $subdept;



	function __construct()
	{
	}

	//setter
	public function setempcode($empcode)
	{
		$this->empcode = $empcode;
	}

	public function setempname($empname)
	{
		$this->empname = $empname;
	}

	public function setdept($dept)
	{
		$this->dept = $dept;
	}
	public function setsubdept($subdept)
	{
		$this->subdept = $subdept;
	}



	//Getter
	public function getempcode()
	{
		return $this->empcode;
	}

	public function getempname()
	{
		return $this->empname;
	}

	public function getdept()
	{
		return $this->dept;
	}
	public function getsubdept()
	{
		return $this->subdept;
	}


	
	public static function checkExist($idno)
	{
		$conn = new Connection2();
		$result = 'false';

		try {
			$conn->open();
			$dataset = $conn->query("SELECT * FROM dbo.HRPRO_Active WHERE HR_Employee_Code ='" . $idno . "'");

			if ($conn->has_rows($dataset)) {

				$result = 'true';
			} else {
				$result = 'false';
			}

			$conn->close();
		} catch (Exception $e) {
		}
		return $result;
	}

	public function CheckID()
	{
		$conn = new Connection2();
		
		try {
			$conn->open();
			$dataset =  $conn->query("SELECT * FROM dbo.HRPRO_Active WHERE HR_Employee_Code ='" . $this->getempcode() . "'");

			if ($conn->has_rows($dataset)) {
				$reader = $conn->fetch_array($dataset);
				$this->setempname($reader["Active_Employee_Name"]);
				$this->setdept($reader["Active_Department"]);
				$this->setsubdept($reader["Active_Section"]);

			}


			$conn->close();
		} catch (Exception $e) {
		}
		
	}
	public static function AllDept()
	{
		$conn = new Connection2();
		$result = array();

		try{
			$conn->open();
			$dataset =  $conn->query("SELECT distinct(Active_Department) FROM dbo.HRPRO_Active  ORDER BY Active_Department" );
			$counter = 0;
			while($reader = $conn->fetch_array($dataset)){

				 $Select = new Idnum();
				 $Select->setdept($reader["Active_Department"]);
				$result[$counter] = $Select;
				$counter++;
			}
					
			$conn->close();
			
		}catch(Exception $e){

		}
		return $result;
	}

	
}
