<?php

include_once("connection.php");

class RACT
{
	private $idno;
	private $fname;
	private $Q1;
	private $Q2;
	private $Q3;
	private $Q4;
	private $Q5;
	private $Q6;
	private $Q7;
	private $Q8;
	private $Q9;
	private $Q10;
	private $Q11;
	private $Q4Remarks;
	private $Q5Remarks;
	private $Q6Remarks;




	function __construct()
	{
	}

	//setter
	public function setidno($idno)
	{
		$this->idno = $idno;
	}

	public function setfname($fname)
	{
		$this->fname = $fname;
	}

	public function setQ1($Q1)
	{
		$this->Q1 = $Q1;
	}
	public function setQ2($Q2)
	{
		$this->Q2 = $Q2;
	}
	public function setQ3($Q3)
	{
		$this->Q3 = $Q3;
	}
	public function setQ4($Q4)
	{
		$this->Q4 = $Q4;
	}
	public function setQ5($Q5)
	{
		$this->Q5 = $Q5;
	}
	public function setQ6($Q6)
	{
		$this->Q6 = $Q6;
	}
	public function setQ7($Q7)
	{
		$this->Q7 = $Q7;
	}
	public function setQ8($Q8)
	{
		$this->Q8 = $Q8;
	}
	public function setQ9($Q9)
	{
		$this->Q9 = $Q9;
	}
	public function setQ10($Q10)
	{
		$this->Q10 = $Q10;
	}
	public function setQ11($Q11)
	{
		$this->Q11 = $Q11;
	}
	public function setQ4Remarks($Q4Remarks)
	{
		$this->Q4Remarks = $Q4Remarks;
	}
	public function setQ5Remarks($Q5Remarks)
	{
		$this->Q5Remarks = $Q5Remarks;
	}
	public function setQ6Remarks($Q6Remarks)
	{
		$this->Q6Remarks = $Q6Remarks;
	}
	



	//Getter
	public function getidno()
	{
		return $this->idno;
	}

	public function getfname()
	{
		return $this->fname;
	}

	public function getQ1()
	{
		return $this->Q1;
	}
	public function getQ2()
	{
		return $this->Q2;
	}
	public function getQ3()
	{
		return $this->Q3;
	}
	public function getQ4()
	{
		return $this->Q4;
	}
	public function getQ5()
	{
		return $this->Q5;
	}
	public function getQ6()
	{
		return $this->Q6;
	}
	public function getQ7()
	{
		return $this->Q7;
	}
	public function getQ8()
	{
		return $this->Q8;
	}
	public function getQ9()
	{
		return $this->Q9;
	}
	public function getQ10()
	{
		return $this->Q10;
	}
	public function getQ11()
	{
		return $this->Q11;
	}
	public function getQ4Remarks()
	{
		return $this->Q4Remarks;
	}
	public function getQ5Remarks()
	{
		return $this->Q5Remarks;
	}
	public function getQ6Remarks()
	{
		return $this->Q6Remarks;
	}
	

	public static function checkExist($idno,$tempdate)
	{
		$conn = new Connection();
		$result = 'false';

		try {
			$conn->open();
			$dataset = $conn->query("SELECT * FROM dbo.Employee_HDF WHERE idno ='" . $idno . "'  and tempdate='".$tempdate."'");

			if ($conn->has_rows($dataset)) {

				$result = 'true';
			} else {
				$result = 'false';
			}

			$conn->close();
		} catch (Exception $e) {
		}
		return $result;
	}


	public function SubRACT()
	{
		$conn = new Connection();

		try {
			$conn->open();
			$conn->query("INSERT INTO dbo.emp_act VALUES('".$this->getidno()."','".$this->getfname()."','".$this->getQ1()."','".$this->getQ2()."','".$this->getQ3()."','".$this->getQ4()."','".$this->getQ5()."','".$this->getQ6()."','".$this->getQ7()."','".$this->getQ8()."','".$this->getQ9()."','".$this->getQ10()."','".$this->getQ11()."','".$this->getQ4Remarks()."','".$this->getQ5Remarks()."','".$this->getQ6Remarks()."',getdate())");
			$conn->close();
		} catch (Exception $e) {
		}
	}



	
	

	
}
