<?php

include_once("connection.php");

class Ques
{
	private $qcode;
	private $qdesc;
	private $qremarks;
	private $rdesc;
	private $dept;
	private $auth;



	function __construct()
	{
	}

	//setter
	public function setqcode($qcode)
	{
		$this->qcode = $qcode;
	}

	public function setqdesc($qdesc)
	{
		$this->qdesc = $qdesc;
	}

	public function setqremarks($qremarks)
	{
		$this->qremarks = $qremarks;
	}

	public function setrdesc($rdesc)
	{
		$this->rdesc = $rdesc;
	}

	public function setdept($dept)
	{
		$this->dept = $dept;
	}

	public function setauth($auth)
	{
		$this->auth = $auth;
	}

	//Getter
	public function getqcode()
	{
		return $this->qcode;
	}

	public function getqdesc()
	{
		return $this->qdesc;
	}

	public function getqremarks()
	{
		return $this->qremarks;
	}

	public function getrdesc()
	{
		return $this->rdesc;
	}

	public function getdept()
	{
		return $this->dept;
	}

	public function getauth()
	{
		return $this->auth;
	}

	public static function GetAllQues()
	{
		$conn = new connection();
		$result = array();

		try{
			$conn->open();
			$dataset =  $conn->query("SELECT * FROM dbo.Question");
			$counter = 0;
			while($reader = $conn->fetch_array($dataset)){
				$Select = new Ques();
				$Select->setqcode($reader["QCode"]);
				$Select->setqdesc($reader["QDesc"]);
				$Select->setqremarks($reader["QRemarks"]);
				$Select->setrdesc($reader["RDesc"]);		
				$result[$counter] = $Select;
				$counter++;
			}

			
			$conn->close();
			
		}catch(Exception $e){

		}
		return $result;
	}




	
	

	
}
