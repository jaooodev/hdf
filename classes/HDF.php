<?php

include_once("connection.php");

class HDF
{
	private $idno;
	private $fname;
	private $temp;
	private $tempdate;
	private $fever;
	private $chills;
	private $cough;
	private $sorethroat;
	private $sob;
	private $bodypain;
	private $lbm;
	private $datefiled;
	private $others;



	function __construct()
	{
	}

	//setter
	public function setidno($idno)
	{
		$this->idno = $idno;
	}

	public function setfname($fname)
	{
		$this->fname = $fname;
	}

	public function settemp($temp)
	{
		$this->temp = $temp;
	}
	public function settempdate($tempdate)
	{
		$this->tempdate = $tempdate;
	}
	public function setfever($fever)
	{
		$this->fever = $fever;
	}
	public function setchills($chills)
	{
		$this->chills = $chills;
	}
	public function setcough($cough)
	{
		$this->cough = $cough;
	}
	public function setsorethroat($sorethroat)
	{
		$this->sorethroat = $sorethroat;
	}
	public function setsob($sob)
	{
		$this->sob = $sob;
	}
	public function setbodypain($bodypain)
	{
		$this->bodypain = $bodypain;
	}
	public function setlbm($lbm)
	{
		$this->lbm = $lbm;
	}
	public function setothers($others)
	{
		$this->others = $others;
	}



	//Getter
	public function getidno()
	{
		return $this->idno;
	}

	public function getfname()
	{
		return $this->fname;
	}

	public function gettemp()
	{
		return $this->temp;
	}
	public function gettempdate()
	{
		return $this->tempdate;
	}
	public function getfever()
	{
		return $this->fever;
	}
	public function getchills()
	{
		return $this->chills;
	}
	public function getcough()
	{
		return $this->cough;
	}
	public function getsorethroat()
	{
		return $this->sorethroat;
	}
	public function getsob()
	{
		return $this->sob;
	}
	public function getbodypain()
	{
		return $this->bodypain;
	}
	public function getlbm()
	{
		return $this->lbm;
	}
	public function getothers()
	{
		return $this->others;
	}

	public static function checkExist($idno,$tempdate)
	{
		$conn = new Connection();
		$result = 'false';

		try {
			$conn->open();
			$dataset = $conn->query("SELECT * FROM dbo.Employee_HDF WHERE idno ='" . $idno . "'  and tempdate='".$tempdate."'");

			if ($conn->has_rows($dataset)) {

				$result = 'true';
			} else {
				$result = 'false';
			}

			$conn->close();
		} catch (Exception $e) {
		}
		return $result;
	}


	public function SubHDF()
	{
		$conn = new Connection();

		try {
			$conn->open();
			$conn->query("INSERT INTO dbo.Employee_HDF VALUES('".$this->getidno()."','".$this->getfname()."','".$this->gettemp()."','".$this->gettempdate()."','".$this->getfever()."','".$this->getchills()."','".$this->getcough()."','".$this->getsorethroat()."','".$this->getsob()."','".$this->getbodypain()."','".$this->getlbm()."',getdate(),'".$this->getothers()."')");
			$conn->close();
		} catch (Exception $e) {
		}
	}

	public static function OtherSymp($idno)
	{

		$conn = new connection();
		$result = array();

		try {
			$conn->open();
			$dataset =  $conn->query("SELECT TOP 15 * from dbo.emp_temp where idno ='" . $idno . "' and others not in ('NA','N/A','NONE')  order by tempdate DESC");
			$counter = 0;
			while ($reader = $conn->fetch_array($dataset)) {

				$Select = new HDF();
				$Select->settempdate($reader['tempdate']->format('F j, Y'));
				$Select->setothers($reader['others']);
				$result[$counter] = $Select;
				$counter++;
			}

			$conn->close();
		} catch (Exception $e) {
		}
		return $result;
	}


	
	

	
}
