<?php

include_once("connection.php");

class Reg
{
	private $idno;
	private $fname;
	private $dept;
	private $subdept;
	private $addtype;
	private $add;
	private $contact;
	private $password;
	private $auth;
	private $img;



	function __construct()
	{
	}

	//setter
	public function setidno($idno)
	{
		$this->idno = $idno;
	}

	public function setfname($fname)
	{
		$this->fname = $fname;
	}

	public function setdept($dept)
	{
		$this->dept = $dept;
	}
	public function setsubdept($subdept)
	{
		$this->subdept = $subdept;
	}
	public function setaddtype($addtype)
	{
		$this->addtype = $addtype;
	}
	public function setadd($add)
	{
		$this->add = $add;
	}
	public function setcontact($contact)
	{
		$this->contact = $contact;
	}
	public function setpassword($password)
	{
		$this->password = $password;
	}
	public function setauth($auth)
	{
		$this->auth = $auth;
	}
	public function setimg($img)
	{
		$this->img = $img;
	}



	//Getter
	public function getidno()
	{
		return $this->idno;
	}

	public function getfname()
	{
		return $this->fname;
	}

	public function getdept()
	{
		return $this->dept;
	}
	public function getsubdept()
	{
		return $this->subdept;
	}
	public function getaddtype()
	{
		return $this->addtype;
	}
	public function getadd()
	{
		return $this->add;
	}
	public function getcontact()
	{
		return $this->contact;
	}
	public function getpassword()
	{
		return $this->password;
	}
	public function getauth()
	{
		return $this->auth;
	}
	public function getimg()
	{
		return $this->img;
	}


	public function RegIdno()
	{
		$conn = new Connection();

		try {
			$conn->open();
			$conn->query("INSERT INTO dbo.employee VALUES('".$this->getidno()."','".$this->getfname()."','".$this->getdept()."','".$this->getsubdept()."','".$this->getcontact()."','".$this->getaddtype()."','".$this->getadd()."',getdate(),'','".password_hash($this->getpassword(), PASSWORD_DEFAULT)."','user',1)");
			$conn->close();
		} catch (Exception $e) {
		}
	}

	public function UpdateProf()
	{
		$conn = new Connection();

		try {
			$conn->open();
			$conn->query("UPDATE  dbo.employee set contact = '".$this->getcontact()."',addresstype ='".$this->getaddtype()."',address='".$this->getadd()."' where idno ='".$this->getidno()."' ");
			$conn->close();
		} catch (Exception $e) {
		}
	}

	public static function checkUser($idno,$pass)
	{
		$conn = new Connection();
		$result = 'false';

		try {
			$conn->open();
			$dataset = $conn->query("SELECT * FROM dbo.employee WHERE idno ='" . $idno . "'");

			if ($conn->has_rows($dataset)) {
				$reader = $conn->fetch_array($dataset);
				$hash = $reader["password"];
				if(password_verify($pass, $hash))
				{
					$result = 'true';
				}
				else
				{
					$result = 'false';
				}
				
			} else {
				$result = 'false1';
			}

			$conn->close();
		} catch (Exception $e) {
		}
		return $result;
	}

	public function UserData($idno)
	{
		$conn = new Connection();
		
		try {
			$conn->open();
			$dataset =  $conn->query("SELECT * FROM dbo.employee WHERE idno ='" .$idno."'");

			if ($conn->has_rows($dataset)) {
				$reader = $conn->fetch_array($dataset);
				$this->setidno($reader["idno"]);
				$this->setfname($reader["fname"]);
				$this->setdept($reader["dept"]);
				$this->setsubdept($reader["subdept"]);
				$this->setsubdept($reader["subdept"]);
				$this->setimg($reader["img"]);
				$this->setauth($reader["auth"]);
				$this->setadd($reader["address"]);
				$this->setcontact($reader["contact"]);
				$this->setpassword($reader["password"]);

			}


			$conn->close();
		} catch (Exception $e) {
		}
		
	}

	public static function checkExist($idno)
	{
		$conn = new Connection();
		$result = 'false';

		try {
			$conn->open();
			$dataset = $conn->query("SELECT * FROM dbo.employee WHERE idno ='" . $idno . "'");

			if ($conn->has_rows($dataset)) {

				$result = 'true';
			} else {
				$result = 'false';
			}

			$conn->close();
		} catch (Exception $e) {
		}
		return $result;
	}
	public function UploadImg($img,$idno)
	{
		$conn = new Connection();

		try {
			$conn->open();
			$conn->query("UPDATE  dbo.employee set img = '".$img."' where idno ='".$idno."' ");
			$conn->close();
		} catch (Exception $e) {
		}
	}

	public function updatePass()
	{
		$conn = new Connection();

		try {
			$conn->open();
			$conn->query("UPDATE  dbo.employee set password = '".password_hash($this->getpassword(), PASSWORD_DEFAULT)."' where idno ='".$this->getidno()."' ");
			$conn->close();
		} catch (Exception $e) {
		}
	}
	

	
}
