
<?php include_once("header.php"); 
session_start();


?>


<main class="mdl-layout__content mdl-color--grey-100">
<div class="page-wrapper">
        
    <div class="container-fluid">
        <div class="container">
            <div class="login-wrap" style="padding-top: 0vh; margin-top: 8vh; padding-bottom: 20px;">
                <div class="login-content">
                    <div class="login-logo"><a href="#"><img src="assets/images/icon/ionics.png" alt="Ionics" style="height:100px;"/></a></div>
                    <div class="login-form">
                        <form method="POST">
                            <div id="success" class="alert alert-success alert-dismissible" role="alert" hidden>
                                <strong>Success!</strong> ID Number is successfully registered!
                            </div>

                            <div id="warning" class="alert alert-warning alert-dismissible" role="alert" hidden>
                                <strong>Warning!</strong>   Please complete necessary details!
                            </div>

                            <div id="danger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Password did not match!
                            </div>

                            <div id="iddanger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   ID Number not found!
                            </div>
                            
                            <div id="first-form">
                                <div class="form-group"><label>ID Number <?php echo $_SESSION['User']; ?></label><input class="au-input" type="text" id="idno" name="idno" placeholder="Id Number" />
                                    <button type="button" class="au-input float-right au-btn--green m-b-20" id="checkID"><p style="color: white">check ID Number</p></button>
                                </div>

                                <div class="form-group"><label>Name</label><input class="au-input au-input--full" type="text" id="fname" name="fname" placeholder="Full Name" disabled="" /></div>
                                <div class="form-group"><label>Department</label><input class="au-input au-input--full" type="text" id="dept" name="dept" placeholder="Department" disabled="" /></div>
                                <div class="form-group"><label>Sub Department</label><input class="au-input au-input--full" type="text" id="subdept" name="subdept" placeholder="Sub Department" disabled="" /></div>
                            </div>

                            <div id="second-form">
                                <div class="form-group">
                                    <label>Address Type:</label>
                                    <Select class="form-control" id="addtype" name="addtype" autofocus>
                                         <option></option>
                                         <option value="permanent">Permanent Address</option>
                                         <option value="rent">Boarding House</option>
                                    </Select>
                                </div>
                                <div class="form-group">
                                    <label>Address:</label>
                                    <input class="au-input au-input--full" type="text" id="add" name="add" placeholder="Address" />
                                </div>
                                <div class="form-group">
                                    <label>Contact Number:</label>
                                    <input class="au-input au-input--full" type="text" id="contact" name="contact" placeholder="Contact Number" />
                                </div>
                                <div class="form-group"><label>Password</label><input class="au-input au-input--full" type="password" id="password1" name="password" placeholder="Password" /></div>

                                <div class="form-group"><label>Confirm Password</label><input class="au-input au-input--full" type="password" id="password2" name="password" placeholder="Confirm Password" /></div>
                                <button class="btn btn-success emp-btn" id ="reg" type="button">Register</button>
                            </div>
                            

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</main>

<?php include_once("footer.php"); ?>
<script src="../assets/js/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

$("#checkID").click(function() {

if(document.getElementById("idno").value=='')
{
    document.getElementById("success").setAttribute("hidden","");
    document.getElementById("warning").setAttribute("hidden","");
    document.getElementById("danger").setAttribute("hidden","");
    document.getElementById("iddanger").setAttribute("hidden","");
    document.getElementById("iddanger").removeAttribute("hidden");
}
else
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var result = this.responseText;
        var res = result.split("_"); 

        //alert(result);
        if(res[0]=='ok')
        {
            document.getElementById("idno").value = res[4];
            document.getElementById("fname").value = res[1];
            document.getElementById("dept").value = res[2];
            document.getElementById("subdept").value = res[3];
            document.getElementById("success").setAttribute("hidden","");
            document.getElementById("warning").setAttribute("hidden","");
            document.getElementById("danger").setAttribute("hidden","");

        }
        else if(res[0]=='notexist')
        {
            document.getElementById("success").setAttribute("hidden","");
            document.getElementById("warning").setAttribute("hidden","");
            document.getElementById("danger").setAttribute("hidden","");
            document.getElementById("iddanger").setAttribute("hidden","");
            document.getElementById("iddanger").removeAttribute("hidden");
        }

    }

  };

  xmlhttp.open("GET", "./php/checkID.php?idno="+document.getElementById("idno").value, true);
  xmlhttp.send();
}
    

});

$("#reg").click(function() {

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var result = this.responseText;
        var res = result.split("_"); 
        alert(result);
        if(document.getElementById("password1").value==document.getElementById("password2").value)
        {
            if(document.getElementById("addtype").value=='' || document.getElementById("add").value=='' || document.getElementById("contact").value=='' || document.getElementById("password1").value=='')
            {
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("warning").removeAttribute("hidden");
            }
            else
            {
                alert("complete");
            }
        }
        else
        {
            document.getElementById("danger").setAttribute("hidden","");
            document.getElementById("danger").removeAttribute("hidden");

        }

    }

  };

  xmlhttp.open("GET", "./php/reg.php?idno="+document.getElementById("idno").value+"&fname="+document.getElementById("fname").value+"&dept="+document.getElementById("dept").value+"&subdept="+document.getElementById("subdept").value+"&addtype="+document.getElementById("addtype").value+"&add="+document.getElementById("add").value+"&contact="+document.getElementById("contact").value+"&password1="+document.getElementById("password1").value, true);
  xmlhttp.send();


});



//        End----------------------------------------------------------------------------------------------------------------------------------

});
</script>




