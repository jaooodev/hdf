<?php
include_once("../classes/HDF.php");
$conn = new Connection();
$data = array();

try {
    $conn->open();
    $result = $conn->query("SELECT * FROM Employee_HDF ");
    while ($row = $conn->fetch_array($result)) {
        $data[] = $row;
    }
    $conn->close();
    echo json_encode($data);
} catch (Exception $e) {
}