<?php 
session_start();
include_once("../classes/Reg.php");

if(isset($_POST['submit'])){
	
	$imgName = $_FILES['file']['name'];
	$imgTmpName = $_FILES['file']['tmp_name'];
	$imgSize = $_FILES['file']['size'];
	$imgError = $_FILES['file']['error'];
	$imgType = $_FILES['file']['type'];

	print_r($_FILES['file']);
	$imgExt = explode('.', $imgName);
	$imgActualExt = strtolower(end($imgExt));

	$allowed = array('jpg','jpeg','png');

	if(in_array($imgActualExt, $allowed))
	{
		if($imgError===0)
		{
			if($imgSize<8388608)
			{
				$NewimgName = uniqid($_SESSION['fname'],true).".".$imgActualExt;
				$ImgDestination = '../assets/images/'.$NewimgName;
				move_uploaded_file($imgTmpName, $ImgDestination);
				$Upload = new Reg();
				$Upload->UploadImg($NewimgName,$_SESSION['User']);
				$_SESSION['img'] = $NewimgName;
				header("location:../public/profile.php?Success=Image Successfully Uploaded!");
			}
			else
			{
				header("location:../public/profile.php?Error=Image too big!");
			}
		}
		else
		{
			header("location:../public/profile.php?Error=There was an error uploading your Image!");
		}
	}
	else
	{
		header("location:../public/profile.php?Error=Image type is not Allowed!");
	}


}

?>