<?php

include_once("../classes/HDF.php");

$idno =$_GET['idno'];
$fname =$_GET['fname'];
$temp =$_GET['temp'];
$tempdate =$_GET['tempdate'];
$fever =$_GET['fever'];
$chills = $_GET['chills'];
$cough = $_GET['cough'];
$sorethroat = $_GET['sorethroat'];
$sob = $_GET['sob'];
$bodypain = $_GET['bodypain'];
$lbm = $_GET['lbm'];
$others = $_GET['others'];

$HDF = new HDF();
$exist = HDF::checkExist($idno,$tempdate);
if($exist=='true')
{
	echo 'tempdateexist_';
}
else
{
	$HDF->setidno($idno);
	$HDF->setfname($fname);
	$HDF->settemp($temp);
	$HDF->settempdate($tempdate);
	$HDF->setfever($fever);
	$HDF->setchills($chills);
	$HDF->setcough($cough);
	$HDF->setsorethroat($sorethroat);
	$HDF->setsob($sob);
	$HDF->setbodypain($bodypain);
	$HDF->setlbm($lbm);
	$HDF->setothers($others);
	$HDF->SubHDF();

echo 'success_';
}


?>