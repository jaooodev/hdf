<?php 

session_start();
include_once("../classes/Reg.php");

if(isset($_POST['btnSignin']))
{
    if(empty($_POST['idno']) || empty($_POST['password']))
    {
    	header("location:../index.php?Empty=Please Fill in the blanks");
    }
    else
    {
    	 $check = Reg::checkUser($_POST['idno'],$_POST['password']);
    	if($check=='true')
    	{
    		$Reg = new Reg();
    		$Reg->UserData($_POST['idno']);

    		$_SESSION['User']=$_POST['idno'];
    		$_SESSION['img']=$Reg->getimg();
    		$_SESSION['dept']=$Reg->getdept();
    		$_SESSION['subdept']=$Reg->getsubdept();
    		$_SESSION['auth']=$Reg->getauth();
    		$_SESSION['fname']=$Reg->getfname();
            $_SESSION['add']=$Reg->getadd();
            $_SESSION['contact']=$Reg->getcontact();
    		header("location:../public/chart.php");
    	}
        else if($check=='false1')
        {
            header("location:../index.php?Empty=User doesn't exist!");
        }
    	else
    	{
    		header("location:../index.php?Empty=User and password did not Match!");
    	}
    }
}
?>
