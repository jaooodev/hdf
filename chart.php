<?php 
session_start();
include_once("layout.php");
include_once("classes/symp.php");
include_once("classes/HDF.php");





 ?>
   <!--  // MAIN CONTENT -->
  <body>
    <div class="page-container"  style="top: 0px;">
        <div class="main-content" style="padding: 20px 20px 20px 20px;">
            <H3 class ="text-center">CHARTS</H3>
            <H6 class ="text-center" style="font-weight: normal;color:#0277bd;"><i>Data for the last 15 days</i></H6><br/>
            <input type="text" id="idno" value=<?php echo $_SESSION['User']; ?> hidden >
<!--             <div class="section__content section__content--p30">
                <div class="container-fluid">
                        <input id="idno" value="<?php echo @$_SESSION['User']; ?>" hidden> 
                        <input id="fname" value="<?php echo @$_SESSION['fname']; ?>" hidden> 
                        <div id="success" class="alert alert-success alert-dismissible" role="alert" hidden>
                            <strong>Success!</strong> Health Declaration successfully submitted!
                        </div>

                        <div id="warning" class="alert alert-warning alert-dismissible" role="alert" hidden>
                            <strong>Warning!</strong>   Please complete necessary details!
                        </div>

                        <div id="danger" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Temperature Date already exist!
                        </div>

                        <table><tr>
                        <div class="form-group">
                            <td><label>Select Date: </label></td>
                            <td><input class="au-input" type="date" id="tempdate" name="tempdate" max="<?php echo date("Y-m-d"); ?>" style="width: 100%; background-color: white;" /></td>
                        </div></tr> 
                        <tr>
                        <div class="form-group">
                            <td style="padding-right: 8px;"><label>Temperature: </label></td>
                            <td><input class="au-input" type="number" step="any" id="temp" name="temp" placeholder="Temparature" style="width: 100%" /></td>      
                        </div></tr>
                       </table> 
                        <div class="card col-md-6">
                            <br/>
                            <div class="card-heading" style="color:#0277bd;">Health Details</div>
                            <div class="card-body">
                              

                            <?php $Symp = Symp::GetAllSymp();
                                    for ($i = 0; $i < count($Symp); $i++) {
                                    ?>
                                       <form>
                                        <div class="form-group">
                                            <label><?php echo $Symp[$i]->getsymp(); ?>:</label>
                                            
                                        </div>

                                        <div class="form-group">

                                            <select name="<?php echo $Symp[$i]->getsymp(); ?>" id="<?php echo $Symp[$i]->getsymp(); ?>" class="form-control" required="required">
                                            <option></option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                            <option value="Mild">Mild</option>
                                          </select>
                                        </div>
                                    </form>
                                    <?php
                                    }
                            ?>
                            <div class="form-group">
                                    <label>Others/Remarks:<BR><h6>(If None, Indicate NA)</h6></label>
                                    <input class="au-input au-input--full" type="text" id="others" name="others" placeholder="Other Illness" />
                                </div>

                        </div>
                        <br/>
                        <br/>
                         <div id="success2" class="alert alert-success alert-dismissible" role="alert" hidden>
                            <strong>Success!</strong> Health Declaration successfully submitted!
                        </div>

                        <div id="warning2" class="alert alert-warning alert-dismissible" role="alert" hidden>
                            <strong>Warning!</strong>   Please complete necessary details!
                        </div>

                        <div id="danger2" class="alert alert-danger alert-dismissible" role="alert" hidden>
                                <strong>Error!</strong>   Temperature Date already exist!
                        </div>
                        </div>
                        <button class="btn btn-success emp-btn" id ="subhdf" type="button">Submit</button><br/>

                       




                    
                </div>
            </div> -->
<canvas id="chartTemp1" width="400" height="400"></canvas>
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script>
 var ctx = document.getElementById("chartTemp1").getContext("2d");
    // examine example_data.json for expected response data
    var json_url = "http://ifactory.ionics-ems.com:8080/hdf/temperature/" +document.getElementById("idno").value;

    // draw empty chart
    var myChart1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: 'Body Tempx',
                data: [],
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    },
                    suggestedMax: 39
                }]
            },
            lineTension: 0
        }
    });

    ajax_chart(myChart1, json_url);

    // function to update our chart
    function ajax_chart(chart, url, data) {
        var data = data || {};
        
        $.getJSON(url, data).done(function(response) {
            // data = response;
            // console.log(response.data[0].date)
            // console.log(response.data[0].temp)
            // console.log(response.data.length);
            // myChart1.data.labels = response.data[0].date;
            // myChart1.data.datasets[0].data = response.data[0].temp; // or you can iterate for multiple datasets
            for(var i = 0 ; i < response.data.length; i++){
                myChart1.data.labels.push(response.data[i].date);
                myChart1.data.datasets[0].data.push(response.data[i].temp);

            }
            
            myChart1.update(); // finally update our chart
        });

    }
</script>



<BR>
<canvas id="chartSymptoms" width="400" height="400"></canvas>
<script>
var ctx = document.getElementById('chartSymptoms').getContext('2d');
 var json_url = "http://ifactory.ionics-ems.com:8080/hdf/symptom/" +document.getElementById("idno").value;
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        //'Fever', 'Chills', 'Cough', 'Sore Throat', 'Short Breath', 'Body Pain', 'LBM'
        labels: [],
        datasets: [{
            label: 'Symptom Pointsx',
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        responsive: true,
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


 ajax_chart(myChart, json_url);

    // function to update our chart
    function ajax_chart(chart, url, data) {
        var data = data || {};
        
        $.getJSON(url, data).done(function(response) {
            // data = response;
            // console.log(response)
            // console.log(response.data[0].symptom)
            // console.log(response.data.length);
            // myChart1.data.labels = response.data[0].date;
            // myChart1.data.datasets[0].data = response.data[0].temp; // or you can iterate for multiple datasets
            for(var i = 0 ; i < response.data.length; i++){
                myChart.data.labels.push(response.data[i].symptom);
                myChart.data.datasets[0].data.push(response.data[i].points);

            }
            
            myChart.update(); // finally update our chart
        });

    }
</script>
</BR>
</br>
<table class="table" style="font-size: 12px">
  <thead>
    <tr>
      <th scope="col">Date</th>
      <th scope="col">Other illness/injuries</th>
    </tr>
  </thead>
  <tbody>
    <?php $other = HDF::OtherSymp($_SESSION['User']);
  for ($i=0; $i < count($other); $i++) 
    { 
                 
    ?>
    <tr>
         <td><?php echo $other[$i]->gettempdate(); ?></td>
        <td><?php echo $other[$i]->getothers(); ?></td>
    <?php } ?>
    </tr>
  </tbody>
</table>

<br/>





        </div>
    </div>
</body>
   <!--  //END MAIN CONTENT
    //END PAGE CONTAINER -->
<?php include_once("footer.php"); ?>

<script src="../assets/js/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

$("#subhdf").click(function() {

if(document.getElementById("tempdate").value=='' || document.getElementById("temp").value=='' || document.getElementById("Fever").value=='' || document.getElementById("Chills").value=='' || document.getElementById("Cough").value=='' || document.getElementById("Sore Throat").value=='' || document.getElementById("Shortness of Breath").value=='' || document.getElementById("Myalgia/Body Pains").value=='' || document.getElementById("Diarrhea/LBM").value=='' || document.getElementById("others").value=='')
    {
        document.getElementById("success").setAttribute("hidden","");
        document.getElementById("warning").setAttribute("hidden","");
        document.getElementById("danger").setAttribute("hidden","");
        document.getElementById("warning").removeAttribute("hidden");
        document.getElementById("success2").setAttribute("hidden","");
        document.getElementById("warning2").setAttribute("hidden","");
        document.getElementById("danger2").setAttribute("hidden","");
        document.getElementById("warning2").removeAttribute("hidden");
    }
    else
    {
        var xmlhttp = new XMLHttpRequest();
         xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var result = this.responseText;
            var res = result.split("_"); 
           //alert(result);
           if(res[0]=='success')
           {
                document.getElementById("tempdate").value='';
                document.getElementById("temp").value='';
                document.getElementById("Fever").value='';
                document.getElementById("Chills").value='';
                document.getElementById("Cough").value='';
                document.getElementById("Sore Throat").value='';
                document.getElementById("Shortness of Breath").value='';
                document.getElementById("Myalgia/Body Pains").value='';
                document.getElementById("Diarrhea/LBM").value='';
                document.getElementById("others").value='';
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("success").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("success2").removeAttribute("hidden");
           }

           else
           {
                document.getElementById("tempdate").value='';
                document.getElementById("temp").value='';
                document.getElementById("Fever").value='';
                document.getElementById("Chills").value='';
                document.getElementById("Cough").value='';
                document.getElementById("Sore Throat").value='';
                document.getElementById("Shortness of Breath").value='';
                document.getElementById("Myalgia/Body Pains").value='';
                document.getElementById("Diarrhea/LBM").value='';
                document.getElementById("others").value='';
                document.getElementById("success").setAttribute("hidden","");
                document.getElementById("warning").setAttribute("hidden","");
                document.getElementById("danger").setAttribute("hidden","");
                document.getElementById("danger").removeAttribute("hidden");
                document.getElementById("success2").setAttribute("hidden","");
                document.getElementById("warning2").setAttribute("hidden","");
                document.getElementById("danger2").setAttribute("hidden","");
                document.getElementById("danger2").removeAttribute("hidden");
           }
        
           

        }

      };

      xmlhttp.open("GET", "./php/submithdf.php?idno="+document.getElementById("idno").value+"&fname="+document.getElementById("fname").value+"&temp="+document.getElementById("temp").value+"&tempdate="+document.getElementById("tempdate").value+"&fever="+document.getElementById("Fever").value+"&chills="+document.getElementById("Chills").value+"&cough="+document.getElementById("Cough").value+"&sorethroat="+document.getElementById("Sore Throat").value+"&sob="+document.getElementById("Shortness of Breath").value+"&bodypain="+document.getElementById("Myalgia/Body Pains").value+"&lbm="+document.getElementById("Diarrhea/LBM").value+"&others="+document.getElementById("others").value, true);
      xmlhttp.send();
    }



});

$("#tempdate").datepicker({
        endDate:'today'
});



//        End----------------------------------------------------------------------------------------------------------------------------------

});
</script>
